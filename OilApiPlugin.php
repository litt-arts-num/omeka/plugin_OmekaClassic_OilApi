<?php
/**
* Open Info List API 
* @author     AnneGF@CNRS
* @license    http://www.gnu.org/licenses/gpl-3.0.txt GNU GPLv3
*/

/**
 * The OIL API plugin
 *
 * @package Oil Api
 */


class OilApiPlugin extends Omeka_Plugin_AbstractPlugin
{
    protected $_filters = array('api_resources');
    public $_hooks = array('config', 'config_form', 'initialize');

    public function filterApiResources($apiResources)
    {
          // For the resource URI: /api/your_resources/[:id]
          $apiResources['siteinfo'] = array(
          // Module associated with your resource.
          'module' => 'oil-api',
          // Controller associated with your resource.
          'controller' => 'site-info',
          // Type of record associated with your resource.
          //'record_type' => 'YourResourceRecord',
          // List of actions available for your resource.
           'actions' => array(
              'index',  // GET request without ID
              //'get',    // GET request with ID
              //'post',   // POST request
              //'put',    // PUT request (ID is required)
              //'delete', // DELETE request (ID is required)
            ),
	   // List of GET parameters available for your index action.
           //'index_params' => array('foo', 'bar'),
    	);
	return $apiResources;
    }

    public function hookInitialize()
    {
      //Add the translations for this very plugin
      add_translation_source(dirname(__FILE__) . '/languages');
    }

    public function hookConfig($args)
    {
      $post = $args['post'];
      set_option('oilapi_resp', $post['oilapi_resp']);
    }

    public function hookConfigForm()
    {
	include('config_form.php');
    }
}
