# OIL API Plugin for Omeka Classic (EN)
/* french version bellow */
Open Info List API Plugin add the resource 'siteinfo' to API to get more information about project and Omeka installation (plugins, themes, etc.).

The plugin configuration page allow to indicate informations about the projet, which will be - as well as a list of active plugins - exposed by the API.


## Requirements
This plugin is for Omeka Classic. The API must be activated in parameters > API ($URL/admin/settings/edit-api).

## Install
Like any plugin:
1. clone or download and unzip this repo in your omeka's plugins/ folder.
2. rename the folder to exactly OilApi
3. go to the Plugins page, and install OIL-API (Open Info List API)

## Warning
The plugin configuration page is currently only a draft.


# OIL API Plugin pour Omeka Classic (FR)
Open Info List API Plugin ajoute la resource 'siteinfo' à l'API afin d'exposer des info supplémentaires concernant le projet et les plugins (actifs).

La page de configuration du plugin peut être afin de compléter certaines informations sur le projet.


## Pré-requis
Ce plugin est conçu pour Omeka Classic et l'API doit être activée dans les paramètres, onglet API ($URL/admin/settings/edit-api).

## Installation
Comme n'importe quel plugin :
1. cloner le dépôt (ou télécharger le plugin et dé-zipper l'archive) dans le sous-dossier plugins/ de votre installation Omeka
2. renommer le dossier en (exactement) OilApi
3. aller sur la page de configuration des plugins et installer « OIL-API (Open Info List API) »

## Attention
La page de configuration du plugin est pour le moment un brouillon incomplet.


# Exemple de sortie Json
Accessible par l'URL $SITE/api/siteinfo

Par rapport au json produit par l'API native ($SITE/api/site), il y a - pour le moment - uniquement "active_plugins" et "project_resp" en plus.
```json
{
   "omeka_url":"http:\/\/archives-plurielles.elan-numerique.fr",
   "omeka_version":"3.1.2",
   "title":"Archives plurielles de la Sc\u00e8ne",
   "description":"Plateforme con\u00e7ue dans le cadre du projet \u00ab M\u00e9moire des spectacles grenoblois \u00bb (UMR 5316 Litt&Arts, Univ. Grenoble Alpes, CNRS) et du WP3 \u00ab Oral history and Performance \u00bb du Performance Lab: Listening to Performance (CDTools, Univ. Grenoble Alpes).\r\n\r\nPour plus d'informations : \r\n> https:\/\/sfr-creation.univ-grenoble-alpes.fr\/fr\/actualites\/archives-vivantes-memoire-spectacles-grenoblois\r\n> https:\/\/performance.univ-grenoble-alpes.fr\/fr\/ecouter-performance-axes-recherche-2022-2025\/histoire-orale-et-performance",
   "author":"Site con\u00e7u et r\u00e9alis\u00e9 par ELAN @Litt&Arts (UMR 5316, Univ. Grenoble Alpes, CNRS) avec la participation de Diandra CRISTACHE (2019) et Laura Fanouillet (2023-2024)",
   "copyright":"",
   "active_plugins":{
      "ExhibitBuilder":"3.6.1",
      "Coins":"2.1.1",
      "CollectionTree":"2.1",
      "GuestUser":"1.1.3",
      "Contribution":"3.2.0.6",
      "ItemOrder":"2.0.2",
      "ItemRelations":"2.1",
      "PdfEmbed":"1.0.1",
      "PdfText":"1.3",
      "SimpleContactForm":"0.6",
      "SimplePages":"3.1.2",
      "RecordRelations":"2.0.1",
      "DublinCoreExtended":"2.2",
      "CSSEditor":"1.1",
      "SimpleVocab":"2.2.2",
      "UniversalViewer":"2.5.9",
      "HistoryLog":"2.10",
      "ArchiveRepertory":"2.15.8",
      "DerivativeImages":"2.0",
      "OilApi":"0.1"
   },
   "project_resp":"Litt&Arts (UMR 5316)"
}
```