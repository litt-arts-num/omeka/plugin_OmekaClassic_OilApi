<?php
/**
 * Oil Api Controller class file
 * 
 * @author  AnneGF@CNRS
 * @license http://www.gnu.org/licenses/gpl-3.0.txt GNU GPLv3
 */

/**
 * The controller for API /siteinfo.
 * 
 * @package OilApi
 */
class OilApi_SiteInfoController extends Omeka_Controller_AbstractActionController
{
    /**
     * Handle GET request without ID.
     */
    //protected $_filters = array('browse_plugins');
        
    public function indexAction()
    {
	$db = get_db();
	$pluginTable = $db->getTable('Plugin');
        $plugins = $pluginTable->findAll();

        $info = array();

        foreach ($plugins as $plugin) {
            $active = $plugin->active == '1';
            $pluginInfo = $plugin->version;
            $name = $plugin->name;
            if ($active) {
                $info[$name] = $pluginInfo;
            }
        }

        $site = array(
            'omeka_url' => WEB_ROOT,
            'omeka_version' => OMEKA_VERSION,
            'title' => get_option('site_title'),
            'description' => get_option('description'),
            'author' => get_option('author'),
	    //'contact' => get_option('administrator_email'),
            'copyright' => get_option('copyright'),
	    'active_plugins' => $info,
	    'project_resp' => get_option('oilapi_resp'),
        );
        $this->_helper->jsonApi($site);
    }
}
