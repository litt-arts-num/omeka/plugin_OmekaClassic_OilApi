<div class="field">
  <div id="primary" class="rows alpha">
    <h2>Description générale du projet</h2>
    <p class="explanation">Responsable, partenaires, présentation...<br/>Domaines, type de fond<br/>Métadonnées</p>
  </div>
  <div class="two columns alpha">
    <label>Responsable du projet</label>
  </div>
  <div class="inputs five columns omega">
    <div class="input-block">
      <input type="text" name="oilapi_resp" value="<?php echo get_option('oilapi_resp'); ?>" />
    </div>
    <p class="explanation"><?php echo __("Contact"); ?> </p>
    <div class="input-block">
      Nom associé au contact
      Email de contact
    </div>
  </div>
  <h2>Extensions, thème, types de contenus</h2>
  <div class="two columns alpha">
    <label>Extension(s) originale(s)</label>
  </div>
  <div class="inputs five columns omega">
    <p class="explanation"><?php echo __("N'indiquer ici que les extension(s) développées dans le cadre de votre projet (nom, description, etc.). La liste exhaustive de vos extensions actives est automatiquement exposée par l'API."); ?> </p>
    <div class="input-block">
      <textarea name="oilapi_originalplugins" rows="8" value="<?php echo get_option('oilapi_originalplugins'); ?>"></textarea>
    </div>
    <p class="explanation"><?php echo __("Types de contenus"); ?> </p>
    <div class="input-block">
      Xxx
    </div>
  </div>
  <h2>Données</h2>
  <div class="two columns alpha">
    <label>Entrée des données</label>
  </div>
  <div class="inputs five columns omega">
    <!--<p class="explanation"><?php echo __("Entrée des données"); ?> </p>-->
    <div class="input-block">
        Xxx
    </div>
  </div>
  <div class="two columns alpha">
    <label>Accès aux données</label>
  </div>
  <div class="inputs five columns omega">
    <div class="input-block">
        Xxx
    </div>
  </div>
  <div class="two columns alpha">
    <label>Licence(s)</label>
  </div>
  <div class="inputs five columns omega">
    <div class="input-block">
        Xxx
    </div>
  </div>
  <div class="two columns alpha">
    <label>Hébergement</label>
  </div>
  <div class="inputs five columns omega">
    <div class="input-block">
        Xxx
    </div>
  </div>
</div>